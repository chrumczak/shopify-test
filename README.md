# Shopify Coding Challenge

## Setup
1. Please fork this repository as a private project at GitLab.
2. We will provide you with the theme URL, ID and password.

## Project tasks

1. On *All Products* collection page not all products are being shown. We are getting some liquid errors there. Can you fix it?
2. Add another field for *Phone Number* to the contact page, and this will be sent with form.
3. There is a section called `sample-section.liquid` that is featured on *Product Pages*. We would like to be able to select it on home page as well, and it should be draggable.
4. Adding products with multiple variants to push-cart works fine, but when you try to add no-variant products, they are not being added. Can you fix it?
5. After logging into account the client would like to redirect customer to Home Page, can you find a simple way to do this?

*Note: Commit each task separately.*
